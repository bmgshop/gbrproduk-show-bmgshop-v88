(function() { //IIFE  
   function getCartArray() {  
     var cartArray = localStorage['cartArray'];  
     if (!cartArray) {  
       cartArray = [];  
       localStorage.setItem('cartArray', JSON.stringify(cartArray));  
     } else {  
       cartArray = JSON.parse(cartArray);  
     }  
     return cartArray.reverse();  
   }  
   var cartArray = getCartArray();  
   // Loop through items in local storage  
   for (var i = 0; i < cartArray.length ; i++) {  
     var key = cartArray[i];  
     var value = localStorage[key];  
     addCartItems(value)

   }  
   // Get item array  
   function getItemArray() {  
     if (!itemArray) {  
       itemArray = []  
     }  
     return itemArray;  
   }  
   var itemArray = getItemArray();  
   // Add cart items to DOM  
   function addCartItems(value) {  
     value = JSON.parse(value);  
     // console.log(value)  
     var item;  
     for (item in value) {  
       var desc = value[0].desc  
       var price = value[1].price  
       var quantity = value[2].qty  
     }  
     var cart = document.querySelector('.cart');  
     var row = document.createElement('li');
     row.style.marginBottom = "16px";  
     var dataDesc = document.createElement('div');  
     dataDesc.innerHTML = '<txt style="color: orange; margin-bottom: -12px; display: block;">+19 Poin di Dapat</txt><br/><img width="80px" src="'+desc+'">';  
     var dataPrice = document.createElement('div');  
     dataPrice.innerHTML = price;  
    dataPrice.style.padding = "6px";
    dataPrice.style.textAlign = "center";
    dataPrice.style.fontSize = "13px";
    dataPrice.style.display = "block";
    dataPrice.style.color = "#555555";
    dataPrice.style.fontStyle = "italic";
     var deleteData = document.createElement('div');  
     var dataQuantity = document.createElement('div');  
     dataQuantity.innerHTML = '<a style="color:#fff;text-decoration:none" href="'+quantity+'"><i class="fas fa-heart"></i> Order - Lihat Lagi</a>'; 
    dataQuantity.style.border = "0";
    dataQuantity.style.padding = "4px 3px";
    dataQuantity.style.background = "#757575";
    dataQuantity.style.color = "#fff";
    dataQuantity.style.fontSize = "12px";
    dataQuantity.style.borderRadius = "3px";
    dataQuantity.style.marginBottom = "9px"; 
     var deleteButton = document.createElement('button')  
     deleteButton.setAttribute('class', key);  
     deleteData.appendChild(deleteButton).innerHTML = 'Hapus';  
     var subTotal = document.createElement('td');  
     subTotal.classList.add('subtotal');  
     subTotal.innerHTML = '$ ' + (price * quantity).toFixed(2);  
     row.appendChild(dataDesc);
        row.appendChild(dataPrice);
        row.appendChild(dataQuantity);
        //row.appendChild(subTotal);
       //row.appendChild(deleteData); 
     cart.appendChild(row);  
   }  
   // Display total amount of items in the cart  
   var itemTotal = cartArray.length;  
   var total = document.querySelector('#history-total');  
   total.innerHTML = itemTotal;  
lastbag = localStorage.getItem('lastbag');
var lastbag = document.querySelector('#lastbag');
lastbag.innerHTML = lastbag;

var newuser = [];
newuser = JSON.parse(localStorage.getItem("newuser"));

   // Display total amount of items in the cart  
   var itemTotalpoin = cartArray.length;  
   var totalpoin = document.querySelector('#poin-total');  
   var pointo = Math.min(Math.max(parseInt(itemTotalpoin), itemTotalpoin * 19), 6000);
  totalpoin.innerHTML = pointo + newuser;
   localStorage.setItem("mypoin", pointo);


   // Add item to Cart  
   var button = document.querySelectorAll('#add');  
   for (var i = 0; i < button.length; i++) {  
     button[i].addEventListener('click', function() {  
       var cartArray = getCartArray();  
       var currentDate = new Date();  
       var key = 'item_' + currentDate.getTime();  
       var itemArray = getItemArray();  
       var description = this.parentElement.querySelector('.post-thumbnail').src;  
       var price = this.parentElement.querySelector('.price').innerHTML;  
       var quantity = this.parentElement.querySelector('.qty').href;  
       itemArray.push({ 'desc': description });  
       itemArray.push({ 'price': price });  
       itemArray.push({ 'qty': quantity });  
       itemArray = JSON.stringify(itemArray)  
       localStorage.setItem(key, itemArray);  
       cartArray.push(key);  
       localStorage.setItem('cartArray', JSON.stringify(cartArray));  
       // Update amount of items in cart  
       itemTotal++;  
       total.innerHTML = itemTotal;  
       ///location.reload(); // Reload page  
     });  
   }  
   var deleteButton = document.querySelector('ul').querySelectorAll('button');  
   for (var i = 0; i < deleteButton.length; i++) {  
     deleteButton[i].addEventListener('click', deleteItem)  
   }  
   // Delete items  
   function deleteItem(e) {  
     var key = e.target.className;  
     var cartArray = getCartArray().reverse();  
     if (cartArray) {  
       for (var i = 0; i < cartArray.length; i++) {  
         if (key == cartArray[i]) {  
           cartArray.splice(0, cartArray.length)  
         }  
       }  
     }  
     localStorage.setItem('cartArray', JSON.stringify(cartArray));  
     location.reload();  
   }  
 })();   
